package com.example.phonepe.model

class QuizModel : ArrayList<QuizDataItem>()

data class QuizDataItem(
    val name: String? = null,
    val imgUrl: String? = null
)