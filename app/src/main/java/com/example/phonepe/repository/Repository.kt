package com.example.phonepe.repository

import androidx.lifecycle.MutableLiveData
import com.example.phonepe.model.QuizModel
import com.example.phonepe.retrofit.ApiClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class Repository {

    fun fetchQuizData(liveData: MutableLiveData<QuizModel>) {
        val call: Call<QuizModel> = ApiClient.getClient.getPostFeed()
        call.enqueue(object : Callback<QuizModel> {
            override fun onResponse(call: Call<QuizModel>, response: Response<QuizModel>) {
                if (response.body() != null && response.body() is QuizModel) {
                    liveData.value = response.body()
                } else {
                    liveData.value = null
                }
            }

            override fun onFailure(call: Call<QuizModel>, t: Throwable) {
                liveData.value = null
            }
        })
    }

}