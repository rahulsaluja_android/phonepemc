package com.example.phonepe.retrofit

import com.example.phonepe.model.QuizModel
import retrofit2.Call
import retrofit2.http.GET

interface APIInterface {

    @GET("/7814e430223c2f59a83e")
    fun getPostFeed(): Call<QuizModel>

}