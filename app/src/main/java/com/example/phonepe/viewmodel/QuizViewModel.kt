package com.example.phonepe.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.phonepe.model.QuizModel
import com.example.phonepe.repository.Repository

class QuizViewModel : ViewModel() {
    private val repository = Repository()

    val quizLiveData: MutableLiveData<QuizModel> = MutableLiveData()

    fun fetchQuizData() {
        repository.fetchQuizData(quizLiveData)
    }

}