package com.example.phonepe.fragment

import android.os.Bundle
import android.view.*
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.phonepe.R
import com.example.phonepe.activity.MainActivity
import com.example.phonepe.adapter.QuizItemAdapter
import com.example.phonepe.databinding.FragmentMainBinding
import com.example.phonepe.model.QuizModel
import com.example.phonepe.viewmodel.QuizViewModel

class MainFragment : Fragment() {
    private lateinit var binding: FragmentMainBinding
    private lateinit var viewModel: QuizViewModel
    private var quizItemAdapter: QuizItemAdapter? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setHasOptionsMenu(true);
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_main, container, false)
        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(context as MainActivity).get(QuizViewModel::class.java)
        attachObserver()
        fetchData()
    }

    private fun attachObserver() {
        viewModel.quizLiveData.observe(viewLifecycleOwner, object : Observer<QuizModel> {
            override fun onChanged(quizModel: QuizModel?) {
                populateView(quizModel)
                viewModel.quizLiveData.removeObserver(this)
            }
        })
    }

    private fun fetchData() {
        binding.fetchStatus = 1
        viewModel.fetchQuizData()
    }

    private fun populateView(quizModel: QuizModel?) {
        if (quizModel.isNullOrEmpty())
            return

        binding.fetchStatus = 2
        binding.recyclerView.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        binding.recyclerView.setHasFixedSize(true)
        quizItemAdapter = QuizItemAdapter()
        quizItemAdapter?.quizDataItemList = quizModel
        binding.recyclerView.adapter = quizItemAdapter
    }

}
