package com.example.phonepe.constant

class Interfaces {

    interface OnItemClickListener {
        fun onClick(position: Int)
    }
}