package com.example.phonepe.adapter

import android.text.InputFilter
import android.text.InputFilter.LengthFilter
import android.text.InputType
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.recyclerview.widget.RecyclerView
import com.example.phonepe.R
import com.example.phonepe.model.QuizDataItem
import com.squareup.picasso.Picasso


class QuizItemAdapter : RecyclerView.Adapter<QuizItemAdapter.MainViewHolder>() {

    lateinit var quizDataItemList: ArrayList<QuizDataItem>

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MainViewHolder {
        val itemView =
            LayoutInflater.from(parent.context).inflate(R.layout.quiz_item, parent, false)
        return MainViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return quizDataItemList.size ?: 0
    }

    override fun onBindViewHolder(holder: MainViewHolder, position: Int) {
        val context = holder.itemView.context

        val quizDataItem = quizDataItemList[position]
        Picasso.with(context).load(quizDataItem.imgUrl).into(holder.icon)

        val name = quizDataItem.name
        name ?: return

        for (i in name.indices) {
            val editText = EditText(context)
            editText.id = i

            val maxLength = 1
            val fArray = arrayOfNulls<InputFilter>(1)
            fArray[0] = LengthFilter(maxLength)
            editText.filters = fArray
            editText.inputType = InputType.TYPE_TEXT_FLAG_CAP_WORDS

            holder.inputContanier.addView(editText)
        }
    }


    inner class MainViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val icon = itemView.findViewById<ImageView>(R.id.quiz_icon)
        val inputContanier = itemView.findViewById<LinearLayout>(R.id.user_input_container)

    }


}
